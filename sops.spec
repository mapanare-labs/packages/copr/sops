%define debug_package %{nil}

Name:    sops
Version: 3.9.2
Release: 0%{?dist}
Summary: sops is an editor of encrypted files
Group:   Applications/System
License: MPL-2.0
Url:     https://github.com/mozilla/%{name}
Source0: %{url}/archive/v%{version}.tar.gz
Source1: %{url}/releases/download/v%{version}/%{name}-v%{version}.linux.amd64

%description
sops is an editor of encrypted files that supports YAML, JSON and BINARY formats
and encrypts with AWS KMS, GCP KMS, Azure Key Vault and PGP.

%prep
%setup -q -n %{name}-%{version}

%install
install -d -m 755 $RPM_BUILD_ROOT%{_bindir}
install -m 755 %{SOURCE1} $RPM_BUILD_ROOT%{_bindir}/%{name}

%files
%license LICENSE
%doc {CHANGELOG.rst,README.rst}
%{_bindir}/%{name}

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Tue Dec 26 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v3.8.1

* Tue Sep 12 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM
